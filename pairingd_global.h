#ifndef PAIRINGD_GLOBAL_H
#define PAIRINGD_GLOBAL_H

/**
 * @file pairingd_global.h
 * @brief Global header of the pairing daemon
 *
 * It defines the GenericPairingDaemon namespace
 */

/**
 * @namespace GenericPairingDaemon
 * @brief Namespace of the generic pairing daemon
 */
namespace GenericPairingDaemon {
    class IpcServer;
    class PairingStore;
    class StaticKeyContainer;

    // These incomplete namespace definitions are useless but included for clearness and completeness
    namespace SharedSecretGeneration{};
    namespace PairingDataExchange{};
    namespace ConvenienceFunctions{};
};

#endif // PAIRINGD_GLOBAL_H
