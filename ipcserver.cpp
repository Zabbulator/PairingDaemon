#include "ipcserver.h"

#include "statickeycontainer.h"
#include "pairingstore.h"
#include "sharedsecretgeneration.h"
#include "conveniencefunctions.h"
#include "pairingdataexchange.h"

#include "../GenericPairingLibrary/genericpairing.h"

#define WAIT_FOR_CONNECTED_MSEC 1000
#define PAIRING_TIMEOUT_MSEC 60000
#define SAS_LENGTH 16 //bit



using GenericPairingDaemon::IpcServer;



IpcServer::IpcServer(StaticKeyContainer* skc, PairingStore* ps) :
    staticKeyContainer(skc), pairingStore(ps)
{}




void IpcServer::readIpcMessage(){
#ifdef Q_OS_ANDROID
    QTcpSocket* localSocket = (QTcpSocket*) sender();
#else
    QLocalSocket* localSocket = (QLocalSocket*) sender();
#endif // Q_OS_ANDROID
    if (!localSocket->canReadLine()) // the reply has only arrived partially so far, this function will be called again when the full answer got transferred
        return;

    QDataStream localStream(localSocket);
    GenericPairing::message_t message_raw;
    localStream >> message_raw;
    GenericPairing::IpcMessage message = (GenericPairing::IpcMessage) message_raw;

    switch (message) {

        case GenericPairing::IpcMessage::Request_Decryption :
            qDebug() << "Got local request IpcRequest_Decryption";
            handleIpcRequest_Decryption(localStream);
            break;

        case GenericPairing::IpcMessage::Request_Signature :
            qDebug() << "Got local request IpcRequest_Signature";
            handleIpcRequest_Signature(localStream);
            break;

        case GenericPairing::IpcMessage::Request_GetPairing :
            qDebug() << "Got local request IpcRequest_GetPairing";
            handleIpcRequest_GetPairing(localStream);
            break;

        case GenericPairing::IpcMessage::Request_GetPublicKey :
            qDebug() << "Got local request IpcRequest_GetPublicKey";
            handleIpcRequest_GetPublicKey(localStream);
            break;

        case GenericPairing::IpcMessage::Request_GetSharedSecret :
            qDebug() << "Got local request IpcRequest_GetSharedSecret";
            handleIpcRequest_GetSharedSecret(localStream);
            break;

        case GenericPairing::IpcMessage::Request_WaitForPairingRequest :
            qDebug() << "Got local request IpcRequest_WaitForPairingRequest";
            handleIpcRequest_WaitForPairingRequest(localStream);
            break;

        case GenericPairing::IpcMessage::Request_SendPairingRequest :
            qDebug() << "Got local request IpcRequest_SendPairingRequest";
            handleIpcRequest_SendPairingRequest(localStream);
            break;

        case GenericPairing::IpcMessage::Request_RemovePairing :
            qDebug() << "Got local request IpcRequest_RemovePairing";
            handleIpcRequest_RemovePairing(localStream);
            break;


        default:
            qDebug() << "Got unknown local request";
            localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Failure << "Unknown request" << "\n";
            break;
    }

#ifdef Q_OS_ANDROID
    localSocket->disconnectFromHost(); // Will wait until all data has been written
#else
    localSocket->disconnectFromServer(); // Will wait until all data has been written
#endif // Q_OS_ANDROID
}










// ================ Request handlers ================




void IpcServer::handleIpcRequest_Decryption(QDataStream& localStream){
    QByteArray ciphertext_raw;
    localStream >> ciphertext_raw;
    localStream.device()->readAll(); // Get rid of the trailing \n
    QString ciphertext(ciphertext_raw);

    std::string plaintext;
    std::string error;
    bool decryptionWorked = staticKeyContainer->decrypt(ciphertext.toStdString(), &plaintext, &error);

    if (decryptionWorked)
        localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Success << plaintext.c_str();
    else
        localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Failure << error.c_str();

    localStream << "\n";
}



void IpcServer::handleIpcRequest_Signature(QDataStream& localStream){
    QByteArray message_raw;
    localStream >> message_raw;
    localStream.device()->readAll(); // Get rid of the trailing \n
    QString message(message_raw);

    std::string signature;
    std::string error;
    bool signingWorked = staticKeyContainer->sign(message.toStdString(), &signature, &error);

    if (signingWorked)
        localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Success << signature.c_str();
    else
        localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Failure << error.c_str();

    localStream << "\n";
}



void IpcServer::handleIpcRequest_GetPairing(QDataStream &localStream){
    handleIpcRequest_GetSharedSecret(localStream);
}



void IpcServer::handleIpcRequest_GetPublicKey(QDataStream &localStream){
    QByteArray contactName_raw;
    localStream >> contactName_raw;
    localStream.device()->readAll(); // Get rid of the trailing \n
    QString contactName(contactName_raw);

    std::string publicKey;
    std::string error;
    bool gotPubKey = pairingStore->getPublicKey(contactName.toStdString(), &publicKey, &error);

    if (gotPubKey)
        localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Success << publicKey.c_str();
    else
        localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Failure << error.c_str();

    localStream << "\n";
}



void IpcServer::handleIpcRequest_GetSharedSecret(QDataStream &localStream){
    QByteArray contactName_raw;
    localStream >> contactName_raw;
    localStream.device()->readAll(); // Get rid of the trailing \n
    QString contactName(contactName_raw);

    std::string sharedSecret;
    std::string error;
    bool gotSharedSecret = pairingStore->getSharedSecret(contactName.toStdString(), &sharedSecret, &error);

    if (gotSharedSecret)
        localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Success << sharedSecret.c_str();
    else
        localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Failure << error.c_str();

    localStream << "\n";
}



void IpcServer::handleIpcRequest_WaitForPairingRequest(QDataStream& localStream){
    QByteArray contactName_raw;
    localStream >> contactName_raw;
    localStream.device()->readAll(); // Get rid of the trailing \n
    QString contactName(contactName_raw);
    if (contactName.isEmpty()){
        qDebug() << "Empty contact name, aborting";
        sendPairingErrorToFrontend(localStream, "Empty contact name");
        return;
    }
    qDebug() << "Contact name:" << contactName;

    // Start listening for TCP messages
    qDebug() << "Starting pairing data exchange server";
    QTcpServer pairingDataExchangeServer;
    if (!pairingDataExchangeServer.listen(QHostAddress::Any, GenericPairing::DefaultPort)){
        qDebug() << "Pairing data exchange server could not listen:" << pairingDataExchangeServer.errorString();
        sendPairingErrorToFrontend(localStream, "Pairing data exchange server could not listen: " + pairingDataExchangeServer.errorString());
    }

    // Wait for pairing request
    qDebug() << "Waiting for incoming connection";
    if (!pairingDataExchangeServer.waitForNewConnection(GenericPairing::DefaultListenTime)){
        qDebug() << "Timeout waiting for pairing request";
        sendPairingErrorToFrontend(localStream, "Timeout waiting for pairing request");
        return;
    }
    qDebug() << "Got connection";

    QTcpSocket* pairingPartnerCommunicationSocket = pairingDataExchangeServer.nextPendingConnection();
    pairingDataExchangeServer.close(); // Only accept the first message, even if it's garbage

    // Wait for Pairing_ClientHash
    qDebug() << "Waiting for Pairing_ClientHash";
    while (!pairingPartnerCommunicationSocket->canReadLine()){
        if (!pairingPartnerCommunicationSocket->waitForReadyRead(GenericPairing::DefaultWaitTime)){
            qDebug() << "Timeout waiting for Pairing_ClientHash";
            sendPairingErrorToFrontend(localStream, "Timeout waiting for Pairing_ClientHash");
            pairingPartnerCommunicationSocket->deleteLater();
            return;
        }
    }

    // Read message
    QByteArray clientHash;
    QString error;
    if (!PairingDataExchange::readMessage_ClientHash(pairingPartnerCommunicationSocket, &clientHash, &error)){
        sendPairingErrorToFrontend(localStream, error);
        pairingPartnerCommunicationSocket->deleteLater();
        return;
    }
    qDebug() << "Got Pairing_ClientHash";

    // Generate own DH private and public key
    qDebug() << "Generating ephemeral DH parameters";
    std::string serverPrivateKey,serverPublicKey;
    SharedSecretGeneration::generateDiffieHellmanParameters(&serverPrivateKey,&serverPublicKey);

    // Send Pairing_ServerPublicKey
    qDebug() << "Sending Pairing_ServerPublicKey";
    PairingDataExchange::sendMessage_ServerPublicKey(pairingPartnerCommunicationSocket, QByteArray::fromStdString(serverPublicKey));

    // Wait for Pairing_ClientPublicKey
    qDebug() << "Waiting for Pairing_ClientPublicKey";
    while (!pairingPartnerCommunicationSocket->canReadLine()){
        if (!pairingPartnerCommunicationSocket->waitForReadyRead(GenericPairing::DefaultWaitTime)){
            qDebug() << "Timeout waiting for Pairing_ClientPublicKey";
            sendPairingErrorToFrontend(localStream, "Timeout waiting for Pairing_ClientPublicKey");
            pairingPartnerCommunicationSocket->deleteLater();
            return;
        }
    }

    // Read message
    QByteArray clientPublicKey;
    if (!PairingDataExchange::readMessage_ClientPublicKey(pairingPartnerCommunicationSocket, &clientPublicKey, &error)){
        qDebug() << error;
        sendPairingErrorToFrontend(localStream, error);
        pairingPartnerCommunicationSocket->deleteLater();
        return;
    }
    qDebug() << "Got Pairing_ClientPublicKey";
    pairingPartnerCommunicationSocket->deleteLater(); // We no longer need the socket

    // Check if hashes match
    if (!(clientHash == QByteArray::fromStdString(ConvenienceFunctions::getSHA256Hash(clientPublicKey.toStdString())))){
        qDebug() << "Client public key hash doesn't match the hash from Pairing_ClientHash, aborting";
        sendPairingErrorToFrontend(localStream, "Client public key hash doesn't match the hash from Pairing_ClientHash");
        return;
    }

    // Create the DH shared secret and save it for the pending pairing
    qDebug() << "Calculating DH shared secret";
    std::string sharedSecret;
    SharedSecretGeneration::agreeOnSharedSecret(serverPrivateKey, clientPublicKey.toStdString(), &sharedSecret);

    // Create SAS and send it to frontend
    sendPairingSasToFrontend(localStream,sharedSecret);

    waitForSasConfirmationAndStorePairing(localStream, contactName.toStdString(), sharedSecret);
}



void IpcServer::handleIpcRequest_SendPairingRequest(QDataStream &localStream){
    QByteArray contactName_raw;
    QByteArray contactIp_raw;
    std::uint16_t contactPort_raw;
    localStream >> contactName_raw >> contactIp_raw >> contactPort_raw;
    localStream.device()->readAll(); // Get rid of the trailing \n
    QString contactName(contactName_raw);
    QString contactIp(contactIp_raw);
    quint16 contactPort(contactPort_raw);
    if (contactName.isEmpty()){
        qDebug() << "Empty contact name, aborting";
        sendPairingErrorToFrontend(localStream,"Empty contact name");
        return;
    }
    qDebug() << "Contact name:" << contactName;
    if (contactIp.isEmpty()){
        qDebug() << "Empty contact IP, aborting";
        sendPairingErrorToFrontend(localStream,"Empty contact IP");
        return;
    }
    qDebug() << "Contact IP:" << contactIp;
    if (!contactPort){
        qDebug() << "Empty contact port, aborting";
        sendPairingErrorToFrontend(localStream,"Empty contact port");
        return;
    }
    qDebug() << "Contact Port:" << contactPort;

    // Open TCP connection to pairing data exchange server
    qDebug() << "Opening TCP connection to pairing data exchange server";
    QTcpSocket* pairingPartnerCommunicationSocket = new QTcpSocket;
    pairingPartnerCommunicationSocket->connectToHost(contactIp,contactPort);
    if (!pairingPartnerCommunicationSocket->waitForConnected(GenericPairing::DefaultWaitTime)){
        qDebug() << "Timeout waiting for TCP connection to be accepted by pairing data exchange server";
        sendPairingErrorToFrontend(localStream, "Timeout waiting for TCP connection to be accepted by pairing data exchange server");
        return;
    }

    // Generate own DH private and public key
    qDebug() << "Generating ephemeral DH parameters";
    std::string clientPrivateKey,clientPublicKey;
    SharedSecretGeneration::generateDiffieHellmanParameters(&clientPrivateKey,&clientPublicKey);

    // Send Pairing_ClientHash
    qDebug() << "Sending Pairing_ClientHash";
    PairingDataExchange::sendMessage_ClientHash(pairingPartnerCommunicationSocket,
                                                QByteArray::fromStdString(ConvenienceFunctions::getSHA256Hash(clientPublicKey)));

    // Wait for Pairing_ServerPublicKey
    qDebug() << "Waiting for Pairing_ServerPublicKey";
    while (!pairingPartnerCommunicationSocket->canReadLine()){
        if (!pairingPartnerCommunicationSocket->waitForReadyRead(GenericPairing::DefaultWaitTime)){
            qDebug() << "Timeout waiting for Pairing_ServerPublicKey";
            sendPairingErrorToFrontend(localStream, "Timeout waiting for Pairing_ServerPublicKey");
            pairingPartnerCommunicationSocket->deleteLater();
            return;
        }
    }

    // Read message
    QByteArray serverPublicKey;
    QString error;
    if (!PairingDataExchange::readMessage_ServerPublicKey(pairingPartnerCommunicationSocket, &serverPublicKey, &error)){
        qDebug() << error;
        sendPairingErrorToFrontend(localStream, error);
        pairingPartnerCommunicationSocket->deleteLater();
        return;
    }
    qDebug() << "Got Pairing_ServerPublicKey";

    // Send Pairing_ClientPublicKey
    qDebug() << "Sending Pairing_ClientPublicKey";
    PairingDataExchange::sendMessage_ClientPublicKey(pairingPartnerCommunicationSocket, QByteArray::fromStdString(clientPublicKey));
    pairingPartnerCommunicationSocket->deleteLater();

    // Create the DH shared secret and save it for the pending pairing
    qDebug() << "Calculating DH shared secret";
    std::string sharedSecret;
    SharedSecretGeneration::agreeOnSharedSecret(clientPrivateKey, serverPublicKey.toStdString(), &sharedSecret);

    // Create SAS and send it to frontend
    sendPairingSasToFrontend(localStream,sharedSecret);

    waitForSasConfirmationAndStorePairing(localStream, contactName.toStdString(), sharedSecret);
}



void IpcServer::handleIpcRequest_RemovePairing(QDataStream &localStream){
    QByteArray contactName_raw;
    localStream >> contactName_raw;
    localStream.device()->readAll(); // Get rid of the trailing \n
    QString contactName(contactName_raw);
    qDebug() << "Contact name:" << contactName;

    qDebug() << "Deleting Pairing\n";
    pairingStore->deletePairing(contactName.toStdString());

    localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Success << "\n";
}










// ================ Helper functions ================




void IpcServer::sendPairingErrorToFrontend(QDataStream& localStream, const QString& error){
    qDebug() << "Sending error message to frontend\n";

    localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Failure
                << error
                << "\n";
}

void IpcServer::sendPairingSasToFrontend(QDataStream& localStream, const std::string& sharedSecret){
    qDebug() << "Creating and sending SAS to frontend";

    std::string sas = ConvenienceFunctions::createSAS(sharedSecret);

    localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Success
                << QByteArray::fromStdString(sas)
                << "\n";
}

void IpcServer::waitForSasConfirmationAndStorePairing(QDataStream& localStream, const std::string& contactName, const std::string& sharedSecret){
    // Wait for user to confirm SAS
    qDebug() << "Waiting for user to confirm SAS";
    while (!localStream.device()->canReadLine()){
        if (!localStream.device()->waitForReadyRead(GenericPairing::DefaultListenTime)){
            qDebug() << "Timeout waiting for user to confirm SAS\n";
            sendPairingErrorToFrontend(localStream, "Timeout waiting for user to confirm SAS");
            return;
        }
    }

    // Read local message
    GenericPairing::message_t message;
    localStream >> message;
    if (!(((GenericPairing::IpcMessage) message) == GenericPairing::IpcMessage::Success)){
        qDebug() << "Expected message Success for SAS confirmation, but got different message\n";
        return;
    }
    qDebug() << "Got SAS confirmation";

    // SAS confirmation was successful, store sharedSecret
    qDebug() << "Storing the pairing\n";
    pairingStore->storePairing(contactName,sharedSecret);
}
