#ifndef CONVENIENCEFUNCTIONS_H
#define CONVENIENCEFUNCTIONS_H

/**
 * @file conveniencefunctions.h
 * @brief Convenience functions for converting data structures and other stuff
 */

#include "pairingd_global.h"

#include <string>

#include <cryptopp/secblock.h>

namespace GenericPairingDaemon {
    /**
     * @namespace GenericPairingDaemon::ConvenienceFunctions
     * @brief Namespace for convenience functions within the generic pairing daemon
     */
    namespace ConvenienceFunctions {
        /**
         * @brief Converts CryptoPP::SecByteBlock to std::string
         * @param secByteBlock to convert
         * @return the resulting std::string
         */
        std::string secByteBlockToStdString(const CryptoPP::SecByteBlock& secByteBlock);
        /**
         * @brief Converts std::string to CryptoPP::SecByteBlock
         * @param stdString to convert
         * @return the resulting CryptoPP::SecByteBlock
         */
        CryptoPP::SecByteBlock stdStringToSecByteBlock(const std::string& stdString);

        /**
         * @brief Base64-encodes a public key
         * @param publicKeyString to encode
         * @return the encoded public key
         */
        std::string publicKeyStringToBase64(const std::string& publicKeyString); // X.509 SubjectPublicKeyInfo string
        /**
         * @brief Decodes a base64-encoded public key
         * @param publicKeyBase64 to decode
         * @return the decoded public key
         */
        std::string publicKeyBase64ToString(const std::string& publicKeyBase64);
        /**
         * @brief DER-encodes a public key
         * @param publicKeyString to encode
         * @return the encoded public key
         */
        std::string publicKeyStringToDER(const std::string& publicKeyString);
        /**
         * @brief BER-decodes a DER-encoded public key
         * @param publicKeyDER to decode
         * @return the decoded public key
         */
        std::string publicKeyDERToString(const std::string& publicKeyDER);

        /**
         * @brief Generates random of given length
         * @param lengthInBytes
         * @return the generated random
         */
        std::string generateRandom(size_t lengthInBytes);

        /**
         * @brief SHA256-hashes given data
         * @param data to hash
         * @return the hash value
         */
        std::string getSHA256Hash(const std::string& data);

        /**
         * @brief Creates fingerprint (short authentication string) of given shared secret
         * @param sharedSecret
         * @return the SAS
         */
        std::string createSAS(const std::string& sharedSecret);

        /**
         * @brief Encrypts data using DER-encoded RSA public key
         * @param unencrypted data to encrypt
         * @param publicKeyDER DER-encoded RSA public key
         * @return the encrypted data
         */
        std::string encryptWithRSAPublicKey(const std::string& unencrypted, const std::string& publicKeyDER);
    };
}

#endif // CONVENIENCEFUNCTIONS_H
