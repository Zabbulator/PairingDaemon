#ifndef STATICKEYCONTAINER_H
#define STATICKEYCONTAINER_H

/**
 * @file statickeycontainer.h
 * @brief Header of the StaticKeyContainer class
 */

#include "pairingd_global.h"

#include <string>

#include <cryptopp/rsa.h>

/**
 * @class GenericPairingDaemon::StaticKeyContainer
 * @brief This class maintains a local static private/public key pair
 *
 * This class provides a local static private/public key pair
 * and all functions that relate it.
 * Currently using RSA keys with the CryptoPP cryptographic library.
 * Because RSA is used, the public key is deriveable from the private key.
 */
class GenericPairingDaemon::StaticKeyContainer
{
public:
    StaticKeyContainer();

    /**
     * @brief Generates private key (saves it in private class member)
     * @param size of private key
     */
    void generatePrivateKey(unsigned int size);
    /**
     * @brief Loads private key from file (saves it in private class member)
     * @param filename
     * @return true if successful, false otherwise
     */
    bool loadPrivateKeyFromFile(const std::string &filename);
    /**
     * @brief Writes private key (from private class member) to file
     * @param filename
     */
    void writePrivateKeyToFile(const std::string &filename);

    /**
     * @brief Runs CryptoPP validation checks
     * @return true if successful, false otherwise
     */
    bool isValidKey();

    /**
     * @brief Returns public key
     * @return public key
     */
    std::string getPublicKeyAsString(); // Full X.509 SubjectPublicKeyInfo saved into a string
    /**
     * @brief Returns public key in base64-encoding
     * @return base64-encoded public key
     */
    std::string getPublicKeyBase64(); // Base64 encoded string from getPublicKeyAsString()
    /**
     * @brief Returns public key in DER-encoding
     * @return DER-encoded public key
     */
    std::string getPublicKeyDEREncoded(); // Only the 'inner' material (PublicKey) of the SubjectPublicKeyInfo, might be incompatible with other libraries


    // RSA-OAEP-SHA256
    /**
     * @brief Decrypts given ciphertext with the private key
     * @param ciphertext to decrypt
     * @param plaintext to hold resulting plaintext
     * @param error to hold errors
     * @return true if successful, false otherwise
     */
    bool decrypt(std::string ciphertext, std::string* plaintext, std::string* error = nullptr);
    /**
     * @brief Signs a message using the private key
     * @param message to sign
     * @param signature to hold resulting signature
     * @param error to hold errors
     * @return true if successful, false otherwise
     */
    bool sign(std::string message, std::string* signature, std::string* error = nullptr);

private:
    /**
     * @brief RSA private key
     *
     * The corresponding public key is derived on need
     */
    CryptoPP::RSA::PrivateKey privateKey;
};

#endif // STATICKEYCONTAINER_H
