#-------------------------------------------------
#
# Project created by QtCreator 2016-03-28T17:14:21
#
#-------------------------------------------------

QT       += core sql network

QT       -= gui

TARGET = pairingd
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

linux:!android {
    message("* Using settings for Unix/Linux.")
    LIBS += -L"/usr/local/lib/"
    LIBS += -L"../build-GenericPairingLibrary-Desktop-Debug/"
}

android {
    message("* Using settings for Android.")
    INCLUDEPATH += "/usr/local/cryptopp/android-armeabi-v7a/include"
    LIBS += -L"/usr/local/cryptopp/android-armeabi-v7a/lib"
    LIBS += -L"../build-GenericPairingLibrary-Android_f_r_armeabi_v7a_GCC_4_9_Qt_5_7_0-Debug"
}

LIBS += -lcryptopp -lgenericpairing

TEMPLATE = app


SOURCES += main.cpp \
    conveniencefunctions.cpp \
    pairingstore.cpp \
    ipcserver.cpp \
    statickeycontainer.cpp \
    pairingdataexchange.cpp \
    sharedsecretgeneration.cpp

HEADERS += \
    conveniencefunctions.h \
    pairingstore.h \
    statickeycontainer.h \
    ipcserver.h \
    pairingdataexchange.h \
    sharedsecretgeneration.h \
    pairingd_global.h

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_EXTRA_LIBS = \
        ../build-GenericPairingLibrary-Android_f_r_armeabi_v7a_GCC_4_9_Qt_5_7_0-Debug/libgenericpairing.so \
}
