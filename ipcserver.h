#ifndef IPCSERVER_H
#define IPCSERVER_H

/**
 * @file ipcserver.h
 * @brief Header of the GenericPairingDaemon::IpcServer class
 */

#include "pairingd_global.h"

#include "statickeycontainer.h"
#include "pairingstore.h"

#include <QtNetwork>

/**
 * @class GenericPairingDaemon::IpcServer
 * @brief This class provides the IPC interface of the pairing daemon
 *
 * Possible requests are:
 *  - Decryption requests
 *  - Signature requests
 *  - Pairing requests
 * as defined in the generic pairing library
 *
 * Communication with local clients uses QByteArrays and std datastructures (instead of QStrings and such)
 * to make it more compatible with clients that don't use Qt
 */
class GenericPairingDaemon::IpcServer : public QObject
{
    Q_OBJECT

public:
    explicit IpcServer(StaticKeyContainer* skc, PairingStore* ps);

    /**
     * @brief Reads an incoming message and starts the corresponding request handler
     */
    void readIpcMessage();

private:
    /**
     * @brief Local static private/public key pair
     */
    StaticKeyContainer* staticKeyContainer;
    /**
     * @brief Storing/Removal of pairing data
     */
    PairingStore* pairingStore;




    // IPC request handlers
    /**
     * @brief Decrypts using the static private key
     * @param localStream for communication with front-end
     */
    void handleIpcRequest_Decryption(QDataStream& localStream);
    /**
     * @brief Signs using the static private key
     * @param localStream for communication with front-end
     */
    void handleIpcRequest_Signature(QDataStream& localStream);
    /**
     * @brief Returns pairing data for given contact to front-end
     * @param localStream for communication with front-end
     */
    void handleIpcRequest_GetPairing(QDataStream& localStream);
    /**
     * @brief Returns public key of given contact to front-end
     * @param localStream for communication with front-end
     */
    void handleIpcRequest_GetPublicKey(QDataStream& localStream);
    /**
     * @brief Returns shared secret of given contact to front-end
     * @param localStream for communication with front-end
     */
    void handleIpcRequest_GetSharedSecret(QDataStream& localStream);
    /**
     * @brief Waits for TCP pairing request and does the pairing data exchange
     *
     * This function starts listening (and waiting) for the pairing data exchange to be
     * initiated by another host.
     * If the pairing request is received within GenericPairing::DefaultListenTime,
     * the pairing data exchange with the host that sent the request starts.
     * The exchange uses ephemeral Diffie-Hellman keys to reach a shared secret.
     * For authentication, a short authentication string (SAS) is derived from the
     * shared secret and sent to the front-end for confirmation by the user.
     * Upon confirmation, the shared secret is stored.
     *
     * @param localStream for communication with front-end
     */
    void handleIpcRequest_WaitForPairingRequest(QDataStream& localStream);
    /**
     * @brief Sends TCP pairing request to given contact and does the pairing data exchange
     *
     * This function initiates the pairing data exchange with a given (through localStream) host.
     * The exchange uses ephemeral Diffie-Hellman keys to reach a shared secret.
     * For authentication, a short authentication string (SAS) is derived from the
     * shared secret and sent to the front-end for confirmation by the user.
     * Upon confirmation, the shared secret is stored.
     *
     * @param localStream for communication with front-end
     */
    void handleIpcRequest_SendPairingRequest(QDataStream& localStream);
    /**
     * @brief Removes pairing data of given contact
     * @param localStream for communication with front-end
     */
    void handleIpcRequest_RemovePairing(QDataStream& localStream);



    // Helper functions
    void setTimerIfNotYetSet();
    void sendPairingErrorToFrontend(QDataStream& localStream, const QString& error);
    void sendPairingSasToFrontend(QDataStream& localStream, const std::string& sharedSecret);
    void waitForSasConfirmationAndStorePairing(QDataStream& localStream, const std::string& contactName, const std::string& sharedSecret);
};

#endif // IPCSERVER_H
