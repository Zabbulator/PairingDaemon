#include "statickeycontainer.h"

#include <cryptopp/osrng.h>
#include <cryptopp/rsa.h>
#include <cryptopp/files.h>
#include <cryptopp/hex.h>
#include <cryptopp/base64.h>
#include <cryptopp/pssr.h>

#include <stdexcept>

using GenericPairingDaemon::StaticKeyContainer;

StaticKeyContainer::StaticKeyContainer()
{}



void StaticKeyContainer::generatePrivateKey(unsigned int size){
    CryptoPP::DefaultAutoSeededRNG rng;
    privateKey.GenerateRandomWithKeySize(rng,size);
}

bool StaticKeyContainer::loadPrivateKeyFromFile(const std::string &filename){
    try {
        CryptoPP::FileSource file(filename.c_str(), true /*pumpAll*/);

        CryptoPP::ByteQueue queue;
        file.TransferTo(queue);
        queue.MessageEnd();

        privateKey.Load(queue);
    }
    catch (CryptoPP::Exception& e){
        std::cerr << e.what() << std::endl;
        return false;
    }
    catch (std::exception& e){
        std::cerr << e.what() << std::endl;
        return false;
    }
    return true;
}

void StaticKeyContainer::writePrivateKeyToFile(const std::string &filename){
    CryptoPP::ByteQueue queue;
    privateKey.Save(queue);

    CryptoPP::FileSink file(filename.c_str());
    queue.CopyTo(file);
    file.MessageEnd();
}



bool StaticKeyContainer::isValidKey(){
    CryptoPP::DefaultAutoSeededRNG rng;
    return privateKey.Validate(rng,3); // Level 3 for throughout security checks
}



std::string StaticKeyContainer::getPublicKeyAsString(){
    CryptoPP::RSA::PublicKey publicKey(privateKey);

    std::string publicKeyAsString;
    CryptoPP::StringSink ss(publicKeyAsString);
    publicKey.Save(ss);

    return publicKeyAsString;
}

std::string StaticKeyContainer::getPublicKeyBase64(){
    std::string publicKeyBase64;

    CryptoPP::StringSource(getPublicKeyAsString(),true,
        new CryptoPP::Base64Encoder(
            new CryptoPP::StringSink(publicKeyBase64),false
        )
    );
    return publicKeyBase64;
}

std::string StaticKeyContainer::getPublicKeyDEREncoded(){
    CryptoPP::RSA::PublicKey publicKey(privateKey);

    std::string publicKeyDEREncoded;
    CryptoPP::StringSink ss(publicKeyDEREncoded);
    publicKey.DEREncode(ss);

    return publicKeyDEREncoded;
}



bool StaticKeyContainer::decrypt(std::string ciphertext, std::string* plaintext, std::string* error){
    CryptoPP::DefaultAutoSeededRNG rng;

    // Create decryptor object
    CryptoPP::RSAES<CryptoPP::OAEP<CryptoPP::SHA256>>::Decryptor decryptor(privateKey);

    // Padding to ensure the correct ciphertext length
    ciphertext.resize(decryptor.FixedCiphertextLength());

    try {
        // Decrypt using CryptoPP pipelining
        CryptoPP::StringSource( ciphertext, true,
            new CryptoPP::PK_DecryptorFilter( rng, decryptor,
                new CryptoPP::StringSink( *plaintext )
            ) // PK_DecryptorFilter
         ); // StringSource
    }
    catch (std::exception& e){
        if (error != nullptr)
            *error = e.what();
        return false;
    }

    return true;
}

bool StaticKeyContainer::sign(std::string message, std::string* signature, std::string* error){
    CryptoPP::DefaultAutoSeededRNG rng;

    // Create signer object
    CryptoPP::RSASS<CryptoPP::PSS, CryptoPP::SHA256>::Signer signer(privateKey);

    // Create signature space
    CryptoPP::SecByteBlock sig(signer.MaxSignatureLength());

    // Sign message
    try {
        size_t length = signer.SignMessage(rng, (const byte*) message.c_str(), message.length(), sig);

        // Resize signature to its true size
        sig.resize(length);

        *signature = (const char*) sig.data();
    }
    catch (std::exception& e){
        if (error != nullptr)
            *error = e.what();
        return false;
    }

    return true;
}
