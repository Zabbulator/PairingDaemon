#ifndef PAIRINGSTORE_H
#define PAIRINGSTORE_H

/**
 * @file pairingstore.h
 * @brief Header of the PairingStore class
 */

#include "pairingd_global.h"

#include <string>

/**
 * @class GenericPairingDaemon::PairingStore
 * @brief This class is responsible for storing and deleting pairing data
 */
class GenericPairingDaemon::PairingStore
{
public:
    /**
     * @brief Sets path where pairing data is stored
     * @param path
     */
    PairingStore(std::string path);

    /**
     * @brief Stores pairing data for given contact
     *
     * Calls either storePublicKey or storeSharedSecret (currently storeSharedSecret)
     *
     * @param contactName
     * @param sharedSecret
     */
    void storePairing(const std::string& contactName,
                      const std::string& sharedSecret);
    /**
     * @brief Stores public key of given contact
     * @param contactName
     * @param publicKeyDEREncoded
     */
    void storePublicKey(const std::string& contactName, const std::string& publicKeyDEREncoded); // Stores full X.509 SubjectPublicKeyInfo
    /**
     * @brief Stores shared secret for given contact
     * @param contactName
     * @param sharedSecret
     */
    void storeSharedSecret(const std::string& contactName, const std::string& sharedSecret);
    /**
     * @brief Removes pairing data of given contact
     * @param contactName
     */
    void deletePairing(const std::string& contactName);

    /**
     * @brief Returns pairing data of given contact
     *
     * Calls either getPublicKey or getSharedSecret (currently getSharedSecret)
     *
     * @param contactName
     * @param pairing
     * @param error to hold errors
     * @return pairing data (shared secret)
     */
    bool getPairing(const std::string& contactName, std::string* pairing, std::string* error); // Currently the paring is a shared secret
    /**
     * @brief Returns public key of given contact
     * @param contactName
     * @param publicKeyString
     * @param error to hold errors
     * @return public key
     */
    bool getPublicKey(const std::string& contactName, std::string* publicKeyString, std::string* error);
    /**
     * @brief Returns shared secret of given contact
     * @param contactName
     * @param sharedSecret
     * @param error to hold errors
     * @return shared secret
     */
    bool getSharedSecret(const std::string& contactName, std::string* sharedSecret, std::string* error);

private:
    /**
     * @brief Path to store pairing data in
     *
     * Is set at construction time
     */
    std::string pathToPairings;
    /**
     * @brief Path to store public keys in
     *
     * Is set to pathToPairings/PublicKeys
     */
    std::string publicKeyPath;
    /**
     * @brief Path to store shared secrets in
     *
     * Is set to pathToPairings/SharedSecrets
     */
    std::string sharedSecretPath;
};

#endif // PAIRINGSTORE_H
