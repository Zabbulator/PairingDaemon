#include "pairingstore.h"

#include <QDir>

#include <cryptopp/rsa.h>
#include <cryptopp/files.h>

using GenericPairingDaemon::PairingStore;

PairingStore::PairingStore(std::string path) :
    pathToPairings(path + "/"), publicKeyPath(path + "/PublicKeys/"), sharedSecretPath(path + "/SharedSecrets/")
{}



void PairingStore::storePairing(const std::string& contactName, const std::string& sharedSecret){
    storeSharedSecret(contactName,sharedSecret);
}

void PairingStore::storePublicKey(const std::string& contactName, const std::string& publicKeyDEREncoded){
    QDir().mkpath(QString::fromStdString(publicKeyPath));

    CryptoPP::RSA::PublicKey publicKey;
    CryptoPP::StringSource ss(publicKeyDEREncoded,true);
    publicKey.BERDecode(ss);

    CryptoPP::ByteQueue queue;
    publicKey.Save(queue);

    CryptoPP::FileSink file( (publicKeyPath + contactName + ".publickey").c_str() );
    queue.CopyTo(file);
    file.MessageEnd();
}

void PairingStore::storeSharedSecret(const std::string& contactName, const std::string& sharedSecret){
    QDir().mkpath(QString::fromStdString(sharedSecretPath));

    CryptoPP::StringSource(sharedSecret,true,
        new CryptoPP::FileSink( (sharedSecretPath + contactName + ".secret").c_str() )
    );
}


void PairingStore::deletePairing(const std::string& contactName){
    std::remove( (publicKeyPath + contactName + ".publickey").c_str() );
    std::remove( (sharedSecretPath + contactName + ".secret").c_str() );
}


bool PairingStore::getPairing(const std::string &contactName, std::string *pairing, std::string *error){
    return getSharedSecret(contactName, pairing, error);
}

bool PairingStore::getPublicKey(const std::string& contactName, std::string* publicKeyString, std::string* error){ // TODO: error handling
    CryptoPP::FileSource file( (publicKeyPath + contactName + ".publickey").c_str(), true );
    CryptoPP::ByteQueue queue;
    file.TransferTo(queue);
    queue.MessageEnd();

    CryptoPP::RSA::PublicKey publicKey;
    publicKey.Load(queue);

    CryptoPP::StringSink ss(*publicKeyString);
    publicKey.Save(ss);

    return true;
}

bool PairingStore::getSharedSecret(const std::string& contactName, std::string* sharedSecret, std::string* error){ // TODO: error handling
    CryptoPP::FileSource file( (sharedSecretPath + contactName + ".secret").c_str(), true,
        new CryptoPP::StringSink(*sharedSecret)
    );
    return true;
}
