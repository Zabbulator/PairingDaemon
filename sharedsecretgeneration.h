#ifndef SHAREDSECRETGENERATION_H
#define SHAREDSECRETGENERATION_H

/** @file sharedsecretgeneration.h
 * @brief Convenience functions for creating a shared secret
 */

#include "pairingd_global.h"

#include <cryptopp/integer.h>

namespace GenericPairingDaemon {
    /**
     * @namespace GenericPairingDaemon::SharedSecretGeneration
     * @brief Namespace for Diffie-Hellman shared secret generation
     */
    namespace SharedSecretGeneration {

        // Both-side generation of shared secret using DH parameters + agreement
        /**
         * @brief Generates an ephemeral Diffie-Hellman private/public key pair
         * @param privateKey to hold private key
         * @param publicKey to hold public key
         * @return true if successful, false otherwise
         */
        bool generateDiffieHellmanParameters(std::string* privateKey, std::string* publicKey);
        /**
         * @brief Compute Diffie-Hellman shared secret
         * @param ownPrivateKey
         * @param partnerPublicKey
         * @param agreedSecret to hold shared secret
         * @return true if successful, false otherwise
         */
        bool agreeOnSharedSecret(const std::string& ownPrivateKey, const std::string& partnerPublicKey, std::string* agreedSecret);





        // Diffie Hellman group parameters
        extern CryptoPP::Integer DiffieHellmanP;
        extern CryptoPP::Integer DiffieHellmanG;
        extern CryptoPP::Integer DiffieHellmanQ;
    };
}

#endif // SHAREDSECRETGENERATION_H
