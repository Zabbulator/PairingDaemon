#ifndef PAIRINGDATAEXCHANGE_H
#define PAIRINGDATAEXCHANGE_H

/**
 * @file pairingdataexchange.h
 * @brief Message types and functions used for pairig data exchange
 */

#include "pairingd_global.h"

#include "../GenericPairingLibrary/genericpairing.h"

#include <QtNetwork>
#include <QString>

namespace GenericPairingDaemon {
    /**
     * @namespace GenericPairingDaemon::PairingDataExchange
     * @brief Namespace for message types and functions related to pairing data exchange
     */
    namespace PairingDataExchange {
        /**
         * @enum Message
         * @brief Message types for pairing data exchange
         */
        enum class Message : GenericPairing::message_t {
            Pairing_ClientHash,
            Pairing_ServerPublicKey,
            Pairing_ClientPublicKey,
        };

        /**
         * @brief Reads ClientHash message and checks for correctness
         * @param pairingPartnerCommunicationSocket for reading message parameters
         * @param clientHash to hold the hash
         * @param error to hold errors
         * @return true if successful, false otherwise
         */
        bool readMessage_ClientHash(QTcpSocket* pairingPartnerCommunicationSocket, QByteArray* clientHash, QString* error);
        /**
         * @brief Reads ServerPublicKey message and checks for correctness
         * @param pairingPartnerCommunicationSocket for reading message parameters
         * @param serverPublicKey to hold the public key
         * @param error to hold errors
         * @return true if successful, false otherwise
         */
        bool readMessage_ServerPublicKey(QTcpSocket* pairingPartnerCommunicationSocket, QByteArray* serverPublicKey, QString* error);
        /**
         * @brief Reads ClientPublicKey message and checks for correctness
         * @param pairingPartnerCommunicationSocket for reading message parameters
         * @param clientPublicKey to hold the public key
         * @param error to hold errors
         * @return true if successful, false otherwise
         */
        bool readMessage_ClientPublicKey(QTcpSocket* pairingPartnerCommunicationSocket, QByteArray* clientPublicKey, QString* error);

        /**
         * @brief Sends ClientHash message
         * @param pairingPartnerCommunicationSocket the communication channel
         * @param clientHash to send
         */
        void sendMessage_ClientHash(QTcpSocket* pairingPartnerCommunicationSocket, const QByteArray& clientHash);
        /**
         * @brief Sends ServerPublicKey message
         * @param pairingPartnerCommunicationSocket the communication channel
         * @param serverPublicKey to send
         */
        void sendMessage_ServerPublicKey(QTcpSocket* pairingPartnerCommunicationSocket, const QByteArray& serverPublicKey);
        /**
         * @brief Sends ClientPublicKey message
         *
         * @param pairingPartnerCommunicationSocket the communication channel
         * @param clientPublicKey to send
         */
        void sendMessage_ClientPublicKey(QTcpSocket* pairingPartnerCommunicationSocket, const QByteArray& clientPublicKey);
    };
}

#endif // PAIRINGDATAEXCHANGE_H
